#include <iostream>
#include <string>
#include <fstream>
#include <algorithm>
#include <vector>
#include <map>
#include <locale>
#include <cstring>
#include <codecvt>

using namespace std;

template<typename Map>
void PrintMap(Map& m)
{
    
    cout << "[ ";
    for (auto &item : m) {
        cout << item.first << ":" << item.second << " ";
    }
    cout << "]\n";
}

string getContentFile(string rute)
{
    string content = "";
    ifstream openfile (rute);
    string line;
    if(openfile.is_open())
    {
        while(! openfile.eof())
        {
            getline(openfile, line);
            content+=line;
        }
    }
    return content;
}

string getAllContentFile(string rute)
{
    string content = "";
    ifstream openfile (rute);
    string line;
    if(openfile.is_open())
    {
        while(! openfile.eof())
        {
            getline(openfile, line);
            content+=line+'\n';
        }
    }
    return content;
}
void print(const string &s)
{
    for (auto it = s.cbegin() ; it != s.cend(); ++it) {
        cout << *it << ' ';
    }
}

string preprocesamiento(string text){
    string preprocesado="";

    for(int i=0;i<text.length()+1;i++){

        if(text[i] == 'j' or text[i]=='h'  or text[i]== 'J'  or text[i]== 'H'){
            preprocesado += 'I';
        }
        else if(text[i]== 'ñ' or text[i]== 'Ñ'){
            preprocesado += 'N';
        }
        else if(text[i]== 'k' or text[i]== 'K'){
            preprocesado += 'L';
        }
        else if(text[i]=='u' or text[i]=='w'  or text[i]== 'U'  or text[i]== 'W'){
            preprocesado += 'V';
        }
        else if(text[i]=='y' or text[i]== 'Y'){
            preprocesado += 'Z';
        }
        else if (text[i]< 65 or text[i]>122) {

        }
        else if (text[i]>= 91 and text[i]<=96){
            // avoid 
        }
        else if (text[i]>=97){
            preprocesado += text[i] - 32;
        }
        else{
            preprocesado+= text[i];
        }
    }
    return preprocesado;
}
bool cmp(pair<char, int>& a,
         pair<char, int>& b)
{
    return a.second < b.second;
}

void frecuencias(string rute){
    string content = getContentFile(rute);
    vector<pair<char,int>> tabla;
    sort(content.begin(),content.end());
    int ite = content[0];
    int cnt = 0;

    for(int i=0;i<content.length();i++){
            cnt += 1; 
            if(ite != content[i]){
                tabla.push_back(pair<char,int>(ite,cnt));
                cnt = 0;
                ite = content[i];
            }
            if(i == content.length() - 1 ){
                tabla.push_back(pair<char,int>(ite,cnt));
                cnt = 0;
            }

    }
    cout <<"Tabla de frecuencias\n";

    PrintMap(tabla);

    std::sort(tabla.begin(), tabla.end(),
            [](const pair<char,int> &l, const pair<char,int> &r)
            {
                if (l.second != r.second) {
                    return l.second > r.second;
                }
 
                return l.first > r.first;
            });
    cout << "El resultado obtenido de los 5 caracteres con mayor frecuencia\n";        
    for(int i=0;i<5;i++){
        cout<<tabla[i].first<<" : "<<tabla[i].second<<endl;
    }
    cout<<endl;
}

void kasiski(string rute){
    string content = getContentFile(rute);
    vector<pair<pair<string,int>,vector<int>>> trigramas;
    string trigrama = "";
    bool exist = false;
    for(int i=0;i<content.length()-3;i++){
        trigrama = "";
        trigrama += content[i] ;
        trigrama += content[i+1] ;
        trigrama += content[i+2] ;

        for(int j=0;j<trigramas.size();j++){
            if(trigrama == trigramas[j].first.first){
                exist = true;
                trigramas[j].first.second+=1;
                trigramas[j].second.push_back(i);
            }
        }
        if (exist == false){
            vector<int>arr;
            arr.push_back(i);
            trigramas.push_back(pair<pair<string,int>,vector<int>>(pair<string,int>(trigrama,1),arr));
        }
        exist=false;
    }
    cout <<"Kasiski\n";
    sort(trigramas.begin(), trigramas.end(),
            [](const pair<pair<string,int>,vector<int>> &l, pair<pair<string,int>,vector<int>> &r)
            {
                if (l.first.second != r.first.second) {
                    return l.first.second  > r.first.second;
                }
 
                return l.first.second > r.first.second;
            });
    for(int i=0; i<3; i++){
        cout << "El trigrama "<<trigramas[i].first.first<<" se repite "<<trigramas[i].first.second << " veces separadas por " ;
        for(int j=0;j<trigramas[i].second.size()-1;j++){
            cout<< trigramas[i].second[j+1] - trigramas[i].second[j]<<" ";
        }
        cout <<"caracteres\n";
    }

}

void unicode_8(string rute){
    string content = getContentFile(rute);
    char hex_string[20];
    string unicode_8 ="0x";
    wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;
    wstring wide = converter.from_bytes(content);
    cout << "\n UNICODE-8 \n";
    for(int i=0;i<wide.length();i++){
        sprintf(hex_string, "%X", wide[i]); 
        unicode_8+= hex_string;
    }
    cout<<unicode_8<<endl;

}

void insertarCadena_20(string rute,string cadena){
    string content = getAllContentFile(rute);
    string cadena_20 = "";
    for(int i=0;i<content.length();i++){
        if(i%20 == 0){
            cadena_20+=cadena;
        }
        cadena_20 += content[i];

    }
    string temp ="";
    for(int i=0;i<cadena_20.length()%4;i++){
        temp+="X";        
    }
    cadena_20 += temp; 
    cout << "\n INSERTANDO AQUÍ \n";
    cout<<cadena_20<<endl;
    
}
int main(){
    string poema = getContentFile("poema.txt");
    ofstream heraldos("HERALDOSNEGROS_pre.txt");
    heraldos << preprocesamiento(poema);
    heraldos.close();
    
    frecuencias("HERALDOSNEGROS_pre.txt");
    kasiski("HERALDOSNEGROS_pre.txt");
    unicode_8("poema.txt");
    insertarCadena_20("poema.txt","AQUÍ");
    return 0;
}   